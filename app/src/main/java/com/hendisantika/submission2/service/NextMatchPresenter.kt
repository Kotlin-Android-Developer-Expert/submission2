package com.hendisantika.submission2.service

import android.util.Log
import com.google.gson.Gson
import com.hendisantika.submission2.api.ApiRepository
import com.hendisantika.submission2.api.TheSportDBApi
import com.hendisantika.submission2.model.EventResponse
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by hendisantika on 14/10/18  20.02.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class NextMatchPresenter(
    private val view: MainView,
    private val apiRepository: ApiRepository,
    private val gson: Gson
) {
    fun getNextMatch(league: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository
                    .doRequest(TheSportDBApi.getNextMatch(league)),
                EventResponse::class.java

            )
            Log.d(
                "api -->", apiRepository
                    .doRequest(TheSportDBApi.getNextMatch(league))
            )
            Log.d("error -->", data.toString())

            uiThread {
                view.hideLoading()
                view.showEventList(data.events)
            }

        }
    }

    fun getNextMatchDetails(eventId: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository
                    .doRequest(TheSportDBApi.getLookUpEvent(eventId)),
                EventResponse::class.java

            )
            Log.d(
                "api -->", apiRepository
                    .doRequest(TheSportDBApi.getLookUpEvent(eventId))
            )
            Log.d("error -->", data.toString())

            uiThread {
                view.hideLoading()
                view.showEventList(data.events)
            }

        }
    }
}