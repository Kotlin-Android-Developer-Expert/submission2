package com.hendisantika.submission2.service

import com.hendisantika.submission2.model.Event

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(data: List<Event>)
}
