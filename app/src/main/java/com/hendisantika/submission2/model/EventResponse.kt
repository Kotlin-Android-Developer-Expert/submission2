package com.hendisantika.submission2.model

/**
 * Created by hendisantika on 14/10/18  12.52.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class EventResponse(val events: List<Event>)