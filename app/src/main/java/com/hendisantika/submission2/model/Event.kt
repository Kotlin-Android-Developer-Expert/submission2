package com.hendisantika.submission2.model

import com.google.gson.annotations.SerializedName

/**
 * Created by hendisantika on 14/10/18  12.43.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Event(
    @SerializedName("idEvent")
    var eventId: String? = null,

    @SerializedName("strHomeTeam")
    var homeTeam: String? = null,

    @SerializedName("strAwayTeam")
    var awayTeam: String? = null,

    @SerializedName("dateEvent")
    var eventDate: String?,

    @SerializedName("intHomeScore")
    var homeScore: Int?,

    @SerializedName("intAwayScore")
    var awayScore: Int?,

    @SerializedName("strHomeGoalDetails")
    var homeGoalDetails: String?,

    @SerializedName("strHomeRedCards")
    var homeRedCards: String?,

    @SerializedName("strHomeYellowCards")
    var homeYellowCards: String?,

    @SerializedName("strHomeLineupGoalkeeper")
    var homeGoalKeeper: String?,

    @SerializedName("strHomeLineupDefense")
    var homeDefense: String?,

    @SerializedName("strHomeLineupMidfield")
    var homeMidField: String?,

    @SerializedName("strHomeLineupForward")
    var homeForward: String?,

    @SerializedName("strAwayRedCards")
    var awayRedCards: String?,

    @SerializedName("strAwayYellowCards")
    var awayYellowCards: String?,

    @SerializedName("strAwayGoalDetails")
    var awayGoalDetails: String?,

    @SerializedName("strAwayLineupGoalkeeper")
    var awayGoalKeeper: String?,

    @SerializedName("strAwayLineupDefense")
    var awayDefense: String?,

    @SerializedName("strAwayLineupMidfield")
    var awayMidField: String?,

    @SerializedName("strAwayLineupForward")
    var awayForward: String?

)