package com.hendisantika.submission2.view

import com.hendisantika.submission2.model.Event

/**
 * Created by hendisantika on 21/10/18  13.00.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface MatchDetailsView {
    fun showLoading()

    fun hideLoading()

    fun getHomeTeamBadge(data: List<Event>)

    fun getAwayTeamBadge(data: List<Event>)

    fun setHomeGoalDetail(goal: String)

    fun setAwayGoalDetail(goal: String)

    fun setHomeRedCards(card: String)

    fun setAwayRedCards(card: String)

    fun setHomeYellowCards(card: String)

    fun setAwayYellowCards(card: String)

    fun setDateTime(date: String)
}