package com.hendisantika.submission2.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.hendisantika.submission2.fragment.LastMatchFragment
import com.hendisantika.submission2.fragment.NextMatchFragment
import com.hendisantika.submission2.fragment.ThirdFragment

/**
 * Created by hendisantika on 14/10/18  10.07.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                LastMatchFragment()
            }
            1 -> NextMatchFragment()
            else -> {
                return ThirdFragment()
            }
        }

    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Last Match"
            1 -> "Next Match"
            else -> {
                return "Third Match"
            }
        }
    }
}