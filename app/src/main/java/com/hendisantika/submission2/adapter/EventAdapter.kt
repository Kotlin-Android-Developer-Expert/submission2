package com.hendisantika.submission2.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hendisantika.submission2.R
import com.hendisantika.submission2.model.Event
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.text.SimpleDateFormat

/**
 * Created by hendisantika on 14/10/18  13.24.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class EventAdapter(private val events: List<Event>, private val listener: (Event) -> Unit) :
    RecyclerView.Adapter<EventAdapter.EventViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): EventViewHolder {
        return EventViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_events, parent, false))
    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bindItem(events[position], listener)
    }

    class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var teamHome: TextView = view.findViewById(R.id.teamHome)
        private var teamAway: TextView = view.findViewById(R.id.teamAway)
        private var score: TextView = view.findViewById(R.id.score)
        private var eventDate: TextView = view.findViewById(R.id.eventDate)

        fun bindItem(event: Event, listener: (Event) -> Unit) {
            teamHome.text = event.homeTeam
            teamAway.text = event.awayTeam

            if (event.homeScore == null && event.awayScore == null) {
                score.text = "VS"
            } else {
                score.text = "${event.homeScore} vs ${event.awayScore}"
            }

            val evDate = SimpleDateFormat("yyyy-MM-dd").parse(event.eventDate)
            val rDate = SimpleDateFormat("EE, dd-MM-yyyy")
            eventDate.text = rDate.format(evDate)

            itemView.onClick {
                listener(event)
            }

        }

    }


}